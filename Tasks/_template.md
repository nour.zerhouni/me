---
layout: default
title:  "{YEAR}.W{WEEK_NUMBER} — {DATE}"
date:   {YEAR}-{MONTH}-{DAY} {TIME} {TZ_OFFSET}
categories: jekyll update
---

# {YEAR}.W{WEEK_NUMBER} — {DATE}

[Current Open Issues](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&assignee_username=mbadeau)

## 🧑‍🏫 Learning Development

Multi-week tasks such as learning or bootcamps.

- [ ] …

---

## 🙋 Commitment

All the tasks I have every intention of completing this week.

- [ ] …

### Zendesk tickets

- [ ] …

---

## 🙆 Unplanned tasks

Things that arose throughout the week that captured my interest or demanded my attention and which I felt the need or capacity to do now.

- [ ] …

---

## 💁 To do later

Tasks which I am giving myself permission to avoid doing at all this week. The checkboxes here are for indicating that these tasks have been scheduled elsewhere.

- [ ] …

---

## 🙅 Tasks to let go of

Tasks that I may have been forwarding along or which I may have initially added to the “To do later” list which I no longer plan to do – if they happen, great, but I’m not actively planning to complete them.

- …

---

## See also

- [Currently assigned tickets](https://gitlab.zendesk.com/agent/search/1?type=ticket&q=assignee%3Ambadeau%40gitlab.com)
